"""
Contains some dummy supporting NLP functions
Contains some supporting ML functions
"""

# Author: Andrei Grinenko <andrey.grinenko@gmail.com>
# License: BSD 3 clause

from collections import defaultdict

def get_all_letter_combinations(list_of_strings, combination_length):
  result_set = set()
  for current_string in list_of_strings:
    for i in range(len(current_string) - combination_length):
      result_set.add(current_string[i:i + combination_length])
  return sorted(list(result_set))


def get_most_common_combinations(list_of_strings, combination_length, max_combinations):
  combination_to_frequency = defaultdict(int)
  for current_string in list_of_strings:
    for i in range(len(current_string) - combination_length):
      combination_to_frequency[current_string[i:i + combination_length]] += 1

  frequent_combinations = sorted(
        combination_to_frequency.items(), key=lambda x: x[1], reverse=True)[:max_combinations]

  return sorted([value[0] for value in frequent_combinations])


def substring_appearance(instance, list_of_substrings):
  result = [1 if substring in instance else 0 for substring in list_of_substrings]
  return result


# Abstract class
class Feature(object):
  """
  Converts instance to sample and provides complementary information
  """
  def __init__(self):
    pass
  def to_feature(self, instance, additional_data):
    raise NotImplementedError
  def get_description(self):
    return ''


class SampleExtractor(object):
  def __init__(self, list_of_features):
    self._list_of_features = list_of_features
  def to_sample(self, instance, additional_data):
    return sum([feature.to_feature(instance, additional_data)
                for feature in self._list_of_features], [])
  def get_description(self):
    return ', '.join([feature.get_description() for feature in self._list_of_features])


# Custom train/valid sets extracting procedure.
# Based on random.sample function from standard library.
# Fixed list of validation samples on increase of train samples number for more stable
# validation checks while tuning ML algorithm
def train_valid_sampling(list_of_samples, list_of_labels, train_number, valid_number,
                         random_seed=None):
  assert(type(list_of_samples) in [tuple, list])
  assert(type(list_of_labels) in [tuple, list])
  assert(train_number == None or train_number + valid_number <= len(population))
  raise NotImplementedError('Add random seed support')
  if train_number == None:
    number_of_choices = valid_number
  else:
    number_of_choices = train_number + valid_number
  train_samples = [None] * train_number
  train_labels = [None] * train_number
  valid_samples = [None] * valid_number
  valid_labels = [None] * valid_number
  algorithm_threshold = 21
  if number_of_choices > 5:
    algorithm_threshold += 4 ** math.ceil(math.log(number_of_choices * 3, 4))
  if len(list_of_samples) <= algorithm_threshold:
    samples_labels_pool = list(zip(list_of_samples, list_of_labels))
    for i in range(valid_number):
      j = random.randrange(0, len(list_of_samples) - i)
      valid_samples[i] = samples_labels_pool[j][0]
      valid_labels[i] = samples_labels_pool[j][1]
      samples_labels_pool[j] = samples_labels_pool[len(list_of_samples) - i - 1]
    if train_number == None:
      train_samples, train_labels = list(zip(*random.shuffle(samples_labels_pool[
                                                           :len(list_of_samples) - valid_number])))
    else:
      for i in range(valid_number, train_number + valid_number):
      j = random.randrange(0, len(list_of_samples) - i)
      train_samples[i - valid_number] = samples_labels_pool[j][0]
      trian_labels[i - valid_number] = samples_labels_pool[j][1]
      samples_labels_pool[j] = samples_labels_pool[len(list_of_samples) - i - 1]

  return train_samples, valid_samples, train_labels, valid_labels

